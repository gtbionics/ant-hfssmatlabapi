% ----------------------------------------------------------------------------
% function hfssCreateReport(fid, ReportName, ReportType, DisplayType, SolutionName,
%                ContextArray, FamiliesArray, ReportDataArray)
% 
% Description :
% -------------
% Creates the VB script to solve a given solution setup.
% 
% Parameters :
% ------------
% fid       - file identifier of the HFSS script file.
% ReportName - String for name
% ReportType - "Model S Parameters", "Terminal S Parameters", "Eigenmode
%               Parameters", "Fields", "Far Feilds", "Near Fields", "Emission Test"
% DisplayType - 
%   For "Model S/Terminal S/Eigenmode Parameters": % "Rectangular Plot", "Polar Plot", "Radiation Pattern", "Smith Chart",
%               "Data Table", "3D Rectangular Plot", or "3D Polar Plot".
%   For "Fields": 
%
% Note :
% ------
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ... 
% hfssCreateReport(fid, stuff);
% ----------------------------------------------------------------------------

function hfssCreateReport(fid, ReportName, ReportType, DisplayType, SolutionName, ...
        ContextArray, FamiliesArray, ReportDataArray)

if ~any(strcmp(ReportType,{'Modal S Parameters', 'Terminal S Parameters', ...
        'Eigenmode Parameters', 'Fields', 'Far Fields', 'Near Fields', 'Emission Test'}))
    error('Not a correct ReportType');
end

if any(strcmp(ReportType, {'Modal S Parameters', 'Terminal S Parameters', 'Eigenmode Parameters'}))
    if ~any(strcmp(DisplayType,{'Rectangular Plot', 'Polar Plot', 'Radiation Pattern', ...
            'Smith Chart', 'Data Table', '3D Rectangular Plot', '3D Polar Plot'}))
        error('Not correct DisplayType for given ReportType');
    end
elseif strcmp(ReportType, 'Fields')
    if ~(strcmp(DisplayType, {'Rectangular Plot', 'Polar Plot', 'Radiation Pattern', ...
            'Data Table', '3D Rectangular pot.'}))
        error('Not correct DisplayType for given ReportType Fields');
    end
elseif any(strcmp(ReportType, {'Far Fields', 'Near Fields'}))
    if ~(strcmp(DisplayType, {'Rectangular Plot', 'Radiation Pattern', ...
            'Data Table', '3D Rectangular Plot', '3D Polar Plot'}))
        error('Not correct DisplayType for given ReportType.');
    end
elseif any(strcmp(ReportType, 'Emission Test'))
    if ~strcmp(ReportType, {'Rectangular Plot', 'Data Table'})
        error('Not correct DisplayType for given ReportType.');
    end
end

strFamiliesArray = strcat('"', FamiliesArray{2}(1), '"');
if length(FamiliesArray{2}) > 1
    for i1 = 2:length(FamiliesArray{2})
        strFamiliesArray = strcat(strFamiliesArray, ', "', FamiliesArray{2}(i1), '"');
    end
end

fprintf(fid, '\n');
fprintf(fid, 'Set oModule = oDesign.GetModule("ReportSetup")\n');
fprintf(fid, 'oModule.CreateReport _\n');
fprintf(fid, '\t"%s", _\n', ReportName);
fprintf(fid, '\t"%s", _\n', ReportType);
fprintf(fid, '\t"%s", _\n', DisplayType);
fprintf(fid, '\t"%s", _\n', SolutionName);
fprintf(fid, '\t%s, _\n', cell2string(ContextArray));
fprintf(fid, '\t%s, _\n', cell2string(FamiliesArray));
fprintf(fid, '\t%s, _\n', cell2string(ReportDataArray));

% if strcmp(DisplayType, 'Rectangular Plot')
%     fprintf(fid, ['\tArray("X Component:=", "%s", "Y Component:=", %s), _\n'], ReportDataArray{1}, cell2string(ReportDataArray{2}));
% elseif any(strcmp(DisplayType, {'Polar Plot','Smith Chart'}))
%     fprintf(fid, ['\tArray("Polar Component:=", %s), _\n'], cell2string(ReportDataArray{1}));
% end

fprintf(fid, '\tArray()\n');

end

function [mString] = cell2string(mCellArray)
    mString = 'Array(';
    for i1 = 1:length(mCellArray)
        if iscell(mCellArray{i1})
            mString = [mString ', ' cell2string(mCellArray{i1})];
        else
            if (1 == i1) % Accounts for comma difference between first elementa and else.
                mString = [mString '"' mCellArray{i1} '"'];
            else
                mString = [mString ', "' mCellArray{i1} '"'];
            end
        end
    end
    mString = [mString ')'];
end
% ----------------------------------------------------------------------------
% function hfssCylinder(fid, Name, Axis, Center, Radius, Height, Units)
% 
% Description :
% -------------
% Creates the VB script necessary to model a cylinder in HFSS.
%
% Parameters :
% ------------
% fid     - file identifier of the HFSS script file.
% Name    - name of the cylinder (in HFSS).
% Center  - center of the cylinder (specify as [x, y, z]). This is also the 
%           starting point of the cylinder.
% Axis    - axis of the cylinder (specify as 'X', 'Y', or 'Z').
% Radius  - radius of the cylinder (scalar).
% Height  - height of the cylidner (from the point specified by Center).
% Units   - specify as 'in', 'mm', 'meter' or anything else defined in HFSS.
% 
% Note :
% ------
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ... 
% hfssCylinder(fid, 'Cyl1', 'Z', [0, 0, 0], 0.1, 10, 'in');
% ----------------------------------------------------------------------------

function hfssCreateCylinder(fid, Name, Axis, Center, Radius, Height, Units)

% Cylinder Parameters.
fprintf(fid, '\n');
fprintf(fid, 'oEditor.CreateCylinder _\n');
fprintf(fid, '\tArray("NAME:CylinderParameters", _\n');
fprintf(fid, ['\t\t"XCenter:=", "' num2str(Center{1}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"YCenter:=", "' num2str(Center{2}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"ZCenter:=", "' num2str(Center{3}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"Radius:=", "' num2str(Radius) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"Height:=", "' num2str(Height) ' ' Units '", _\n']);
fprintf(fid, '\t\t"WhichAxis:=", "%s"), _\n', upper(Axis));

% Cylinder Properties.
fprintf(fid, '\tArray("NAME:Attributes", _\n'); 
fprintf(fid, '\t\t"Name:=", "%s", _\n', Name);
fprintf(fid, '\t\t"Flags:=", "", _\n');
fprintf(fid, '\t\t"Color:=", "(132 132 193)", _\n');
fprintf(fid, '\t\t"Transparency:=", 0, _\n');
fprintf(fid, '\t\t"PartCoordinateSystem:=", "Global", _\n');
fprintf(fid, '\t\t"MaterialName:=", "vacuum", _\n');
fprintf(fid, '\t\t"SolveInside:=", true)\n');
fprintf(fid, '\n');

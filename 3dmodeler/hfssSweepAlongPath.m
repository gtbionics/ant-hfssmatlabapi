% ----------------------------------------------------------------------------
% function hfssSweepAlongVector(fid, Object2D, Axis, SweepAngle, ...
%                             [DraftAngle = 0 deg], [DraftType = 'Round'])
%
% Description:
% ------------
% Creates the VB Script necessary to sweep a 2D object around the given axis
% to create a 3D object. 
% 
% Parameters:
% -----------
% fid        - file identifier of the HFSS script file.
% Object2D   - name of the 2D Object to be sweeped.
% SweepAngle - angle (in *deg*) over which the object is to be sweeped.
% DraftAngle - angle (in *deg*) to which the object's profile, or shape is 
%              expanded (or contracted) as it is swept.
% DraftType  - set it to either 'Round' (default), 'Extended' or 'Natural'
%              (consult the HFSS Help for more info).
%
% ----------------------------------------------------------------------------

function hfssSweepAlongPath(fid, ObjectList, DraftAngle, DraftType, TwistAngle)

% arguments processor.
if (nargin < 2)
	error('Not enough arguments !');
elseif (nargin < 3)
	DraftAngle = 0;
	DraftType = 'Round';
    TwistAngle = 0;
elseif (nargin < 4)
	DraftType = 'Round';
    TwistAngle = 0;
elseif (nargin < 5)
    TwistAngle = 0;
end

% default arguments.
if isempty(DraftAngle)
	DraftAngle = 0;
end
if isempty(DraftType)
	DraftType = 'Round';
end
if isempty(TwistAngle)
	TwistAngle = 0;
end

fprintf(fid, '\n');

fprintf(fid, 'oEditor.SweepAlongPath _\n');
fprintf(fid, '\tArray("NAME:Selections", "Selections:=", ');

% Total # of Objects.
nObjects = length(ObjectList);

% Add the Objects.
fprintf(fid, '"');
for iP = 1:nObjects-1,
	fprintf(fid, '%s,', ObjectList{iP});
end
fprintf(fid, [num2str(ObjectList{nObjects}) '"), _\n']);

fprintf(fid, '\tArray("NAME:PathSweepParameters", _\n');
fprintf(fid, '\t\t"DraftAngle:=", "%0.2f deg", _\n', DraftAngle);
fprintf(fid, '\t\t"DraftType:=", "%s", _\n', DraftType);
fprintf(fid, '\t\t"TwistAngle:=", "%0.2f deg" _\n', TwistAngle);
fprintf(fid, '\t)');
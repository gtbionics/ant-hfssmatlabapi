% -------------------------------------------------------------------------- %
% function hfssHelix(fid, Objects, Center, StartDir, Thread, NumThread, RightHand, Units)
% Description:
% ------------
%
% Parameters:
% -----------
% Name - Name Attribute for the PolyLine.
% Units - can be either:
%         'mm' - millimeter.
%         'in' - inches.
%         'mil' - mils.
%         'meter' - meter (note: don't use 'm').
%          or anything that Ansoft HFSS supports.
%
% Example:
% --------
% -------------------------------------------------------------------------- %

function hfssCreateHelix(fid, Objects, Center, StartDir, Thread, NumThread, RightHand, Units)

% Preamble.
fprintf(fid, '\n');
fprintf(fid, 'oEditor.CreateHelix _\n');
fprintf(fid, '\tArray("NAME:Selections", "Selections:=", ');

nObjects = length(Objects);

fprintf(fid, '"');
for iP = 1:nObjects-1,
	fprintf(fid, '%s,', Objects{iP});
end;
fprintf(fid, '%s"), _\n', Objects{nObjects});

fprintf(fid, '\tArray("NAME:HelixParameters", _\n');
fprintf(fid, ['\t\t"XCenter:=", "' num2str(Center{1}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"YCenter:=", "' num2str(Center{2}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"ZCenter:=", "' num2str(Center{3}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"XStartDir:=", "' num2str(StartDir{1}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"YStartDir:=", "' num2str(StartDir{2}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"ZStartDir:=", "' num2str(StartDir{3}) ' ' Units '", _\n']);
fprintf(fid, ['\t\t"Thread:=", "' num2str(Thread) '", _\n']);
fprintf(fid, ['\t\t"NumThread:=", "' num2str(NumThread) '", _\n']);
fprintf(fid, ['\t\t"RightHand:=", ' RightHand ' _\n']);
fprintf(fid, '\t)\n');
% ----------------------------------------------------------------------------
% function hfssPaste(fid)
% 
% Description :
% -------------
% Creates VB Script necessary to copy HFSS objects.
%
% Parameters :
% ------------
% fid   - file identifier of the HFSS script file.
% 
% Note :
% ------
% Recall that pasting 'Layers' creates the name 'Layers1', etc.
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ... 
% hfssPaste(fid);
% ----------------------------------------------------------------------------

function hfssPaste(fid)

fprintf(fid, '\n');
fprintf(fid, 'oEditor.Paste\n');
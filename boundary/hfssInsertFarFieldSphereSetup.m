% ----------------------------------------------------------------------------
% function hfssInsertFarFieldSphereSetup(fid, SetupName, UseCustomRadiationSurface, ...
%   CustomRadiationSurface, Theta, Phi, UseLocalCS, CoordSystem)
% 
% Description :
% -------------
% Creates the VB Script necessary to assign the radiation boundary condition
% to a (closed) Object.
%
% Parameters :
% ------------
% fid     - file identifier of the HFSS script file.
% Name    - name of the radiation boundary condition (under HFSS).
% Object  - object to which the radiation boundary conditions needs to be 
%           applied.
% 
% Note :
% ------
% This function cannot be used to apply radiation boundary conditions to 
% individual faces of an object.
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ... 
% hfssAssignRadiation(fid, 'ABC', 'AirBox');
% ----------------------------------------------------------------------------

function hfssInsertFarFieldSphereSetup(fid, SetupName, UseCustomRadiationSurface, ...
    CustomRadiationSurface, Theta, Phi, UseLocalCS, CoordSystem)

fprintf(fid, '\n');
fprintf(fid, 'Set oModule = oDesign.GetModule("RadField")\n');
fprintf(fid, 'oModule.InsertFarFieldSphereSetup _\n');
fprintf(fid, '\tArray(');
fprintf(fid, '"NAME:%s", _\n', SetupName);
fprintf(fid, '\t"UseCustomRadiationSurface:=", %s, _\n', UseCustomRadiationSurface);
fprintf(fid, '\t"CustomRadiationSurface:=", "%s", _\n', CustomRadiationSurface);
fprintf(fid, '\t"ThetaStart:=", "%s", _\n', Theta{1});
fprintf(fid, '\t"ThetaStop:=", "%s", _\n', Theta{2});
fprintf(fid, '\t"ThetaStep:=", "%s", _\n', Theta{3});
fprintf(fid, '\t"PhiStart:=", "%s", _\n', Phi{1});
fprintf(fid, '\t"PhiStop:=", "%s", _\n', Phi{2});
fprintf(fid, '\t"PhiStep:=", "%s", _\n', Phi{3});
fprintf(fid, '\t"UseLocalCS:=", %s, _\n', UseLocalCS);
fprintf(fid, '\t"CoordSystem:=", "%s")\n', CoordSystem);
